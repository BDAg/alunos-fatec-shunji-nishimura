## NOME_ALUNO

<p align="right">
  <img src="./Imagens/Exemplo/Exemplo.jpg" width="150" title="NOME_ALUNO" style="border-radius: 50%;">
</p>

Cidade:    
Estado:    
País:     
Idade:   
Linkedin: []()  

### Formação Acadêmica

* **FORMAÇÃO_ACADEMICA**
  Período: de dd/mm/aaaa até dd/mm/aaaa (atualmente)

### Experiência Profissional

* **CARGO, LOCAL DE TRABALHO, CIDADE**
  Período: PERIODO_PROFISSIONAL

### Línguas Estrangeiras

* **LINGUA_ESTRANGEIRA (Básico/Intermediário/Avançado)**

### Habilidades Técnicas

* **HABILIDADES_TECNICAS**

### Competências

* **COMPETENCIAS**

### Perfil DISC

* **PERFIL_DISC**

### Matriz De Habilidades (Inicio do Projeto)

<img src="./Imagens/Exemplo/MatrizHabilidadesInicio.png" title="Matriz De Habilidades NOME_ALUNO Inicio">

### Matriz De Habilidades (Final do Projeto)

<img src="./Imagens/Exemplo/MatrizHabilidadesFinal.png" title="Matriz De Habilidades NOME_ALUNO Inicio">